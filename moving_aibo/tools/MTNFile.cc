//
// Copyright 2002 Sony Corporation 
//
// Permission to use, copy, modify, and redistribute this software for
// non-commercial use is hereby granted.
//
// This software is provided "as is" without warranty of any kind,
// either expressed or implied, including but not limited to the
// implied warranties of fitness for a particular purpose.
//

#include <string.h>
#include "MTNFile.h"
#include <stdio.h>

word
MTNFile::GetNumJoints()
{
    return (GetSection2())->numJoints;
}

word
MTNFile::GetNumKeyFrames()
{
    return section0.numKeyFrames;
}

word
MTNFile::GetFrameRate()
{
    return section0.frameRate;
}

char*
MTNFile::GetName()
{
    MTNString* motion = &(section1.motion);
    return string_access(motion);
}

char*
MTNFile::GetAuthor()
{
    MTNString* tmp = &(section1.motion);
    MTNString* author = (MTNString*)((byte*)tmp->name + tmp->length);
    return string_access(author);
}

char*
MTNFile::GetRobotDesign()
{
    MTNString* tmp = &(section1.motion);
    tmp = (MTNString*)((byte*)tmp->name + tmp->length);
    MTNString* design = (MTNString*)((byte*)tmp->name + tmp->length);
    return string_access(design);
}

char*
MTNFile::GetLocator(int index)
{
    MTNString* locator = (GetSection2())->locator;
    for (int i = 0; i < index; i++)
        locator = (MTNString*)((byte*)locator->name + locator->length);

    return string_access(locator);
}

MTNString*
MTNFile::GetLocator2(int index)
{
    MTNString* locator = (GetSection2())->locator;
    for (int i = 0; i < index; i++)
        locator = (MTNString*)((byte*)locator->name + locator->length);

    return locator;
}

longword
MTNFile::GetDataType()
{
    return (GetSection3())->dataType;
}

int
MTNFile::GetEachKeyFrameSize()
{
    int sizeofKeyFrame = (3 + GetNumJoints()) * sizeof(slongword);
    return sizeofKeyFrame;
}

int
MTNFile::GetTotalKeyFrameSize()
{
    int numKeyFrames = GetNumKeyFrames();
    int keyFrameSize = GetEachKeyFrameSize() * numKeyFrames
        + sizeof(slongword) * (numKeyFrames-1); // numInterpolate
    return keyFrameSize;
}

MTNKeyFrame*
MTNFile::GetKeyFrame(int index)
{
    byte* ptr = (GetSection3())->keyFrame;
    ptr = ptr + (GetEachKeyFrameSize() + sizeof(slongword)) * index;
    return (MTNKeyFrame*)ptr;
}

int
MTNFile::GetNumInterpolate(int index)
{
    if (index >= GetNumKeyFrames() - 1) return -1;

    byte* ptr = (GetSection3())->keyFrame;
    int offset = GetEachKeyFrameSize() + sizeof(slongword);
    ptr = ptr + (index + 1) * offset - 4;
    return (int)*((int*)ptr);
}

int
MTNFile::GetNumInterpolate8ms(int index)
{
    int n = GetNumInterpolate(index);
    return 2 * n + 1;
}

slongword
MTNFile::GetJointValue(int index, int jointIndex)
{
    MTNKeyFrame* kf = GetKeyFrame(index);
    return kf->data[jointIndex];
}

MTNSection2*
MTNFile::GetSection2()
{
    byte* ptr = (byte*)&section1;
    int offset = section1.sectionSize;
    return (MTNSection2*)(ptr + offset);
}

MTNSection3*
MTNFile::GetSection3()
{
    byte* ptr = (byte*)GetSection2();
    int offset = (GetSection2())->sectionSize;
    return (MTNSection3*)(ptr + offset);
}

char*
MTNFile::string_access(MTNString* mstr)
{
    static char buffer[256];    // magic number !! Improve later

    memcpy(buffer, mstr->name, mstr->length);
    buffer[mstr->length] = 0;
    return buffer;
}
void
MTNFile::PrintKeyFrame(int index)
{

    char* mtnstring = string_access(GetLocator2(index));
    printf("string: %s",mtnstring);
}
void
MTNFile::Print()
{
    printf("%s\n", GetName() );
    printf("%s\n", GetAuthor() );
    printf("%s\n", GetRobotDesign() );
    printf("Number of joints: %d\n", GetNumJoints() );
    printf("Number of frames: %d\n", GetNumKeyFrames());
    printf("Frame rate: %d\n", GetFrameRate());

    for (int i=0; i<GetNumKeyFrames() - 1 ; i++ )
    {
        printf("Printing joint values for keyframe number %d ========\n", i );
        for (int j=0; j<GetNumJoints() - 1; j++)
            printf("Joint %s value: %f\n", GetLocator(j), ((double)GetJointValue(i,j))/1000000.0 );
    }
}

void
MTNFile::ConvertForROS(std::string filename)
{

    FILE* fp = fopen(filename.c_str(), "w");
    fprintf(fp,"%d\n", GetNumKeyFrames());
    fprintf(fp,"%d\n", GetFrameRate());
    fprintf(fp,"%d\n", GetNumJoints());

    for (int i=0; i<GetNumKeyFrames()  ; i++ )
    {
        fprintf(fp, "%d\n", i);
        for (int j=0; j<GetNumJoints() ; j++)
        {
            std::string joint = std::string (GetLocator(j)); 
            ConvertJointName(joint);
            fprintf(fp, "%s\n", joint.c_str() );
            fprintf(fp, "%f\n", ((double)GetJointValue(i,j))/1000000.0 );
        }
    }

    fclose(fp);
}

void
MTNFile::ConvertJointName (std::string& joint)
{

    if (joint == "PRM:/r1/c1-Joint2:11")
    {
        joint = "neck_joint";
        return;
    }
    if (joint == "PRM:/r1/c1/c2-Joint2:12")
    {
        joint = "headPan";
        return;
    }
    if (joint == "PRM:/r1/c1/c2/c3-Joint2:13")
    {
        joint = "headTilt";
        return;
    }
    if (joint == "PRM:/r1/c1/c2/c3/c4-Joint2:14")
    {
        joint = "mouth_joint";
        return;
    }
    if (joint == "PRM:/r1/c1/c2/c3/e5-Joint4:15")
    {
        joint = "L_ear_joint";
        return;
    }
    if (joint == "PRM:/r1/c1/c2/c3/e6-Joint4:16")
    {
        joint = "R_ear_tilt";
        return;
    }
    if (joint == "PRM:/r2/c1-Joint2:21")
    {
        joint = "legLF1";
        return;
    }
    if (joint == "PRM:/r2/c1/c2-Joint2:22")
    {
        joint = "legLF2";
        return;
    }
    if (joint == "PRM:/r2/c1/c2/c3-Joint2:23")
    {
        joint = "legLF3";
        return;
    }
    if (joint == "PRM:/r3/c1-Joint2:31")
    {
        joint = "legLB1";
        return;
    }
    if (joint == "PRM:/r3/c1/c2-Joint2:32")
    {
        joint = "legLB2";
        return;
    }
    if (joint == "PRM:/r3/c1/c2/c3-Joint2:33")
    {
        joint = "legLB3";
        return;
    }
    if (joint == "PRM:/r4/c1-Joint2:41")
    {
        joint = "legRF1";
        return;
    }
    if (joint == "PRM:/r4/c1/c2-Joint2:42")
    {
        joint = "legRF2";
        return;
    }
    if (joint == "PRM:/r4/c1/c2/c3-Joint2:43")
    {
        joint = "legRF3";
        return;
    }
    if (joint == "PRM:/r5/c1-Joint2:51")
    {
        joint = "legRB1";
        return;
    }
    if (joint == "PRM:/r5/c1/c2-Joint2:52")
    {
        joint = "legRB2";
        return;
    }
    if (joint == "PRM:/r5/c1/c2/c3-Joint2:53")
    {
        joint = "legRB3";
        return;
    }
    if (joint == "PRM:/r6/c1-Joint2:61")
    {
        joint = "tailTilt";
        return;
    }
    if (joint == "PRM:/r6/c2-Joint2:62")
    {
        joint = "tailPan";
        return;
    }

}
